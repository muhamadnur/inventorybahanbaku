/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import auth.Login;
import auth.User;
import form.FormGantiPassword;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import koneksi.MyConnection;
import menu.MenuItem;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;
import panel.DataBahanPanel;
import panel.DataLoketPanel;
import panel.DataPenggunaPanel;
import panel.DataKemasanPanel;
import panel.DataSuppliyerPanel;
import panel.histori.HistoriBahanKeluarPanel;
import panel.histori.HistoriBahanMasukPanel;
import panel.histori.HistoriProdukKeluarPanel;
import panel.histori.HistoriProdukMasukPanel;
import panel.stok.StokBahanPanel;
import panel.stok.StokProdukPanel;
import panel.transaksi.PanelBahanKeluar;
import panel.transaksi.PanelBahanMasuk;
import panel.transaksi.PanelProdukKeluar;
import panel.transaksi.PanelProdukMasuk;

/**
 *
 * @author Admin
 */
public class Main extends javax.swing.JFrame {

    PreparedStatement ps;
    ResultSet rs;
    String id_user = User.getId_user();
    String name = User.getName();
    String username = User.getUsername();
    int role_id = User.getRole_id();
    JpanelLoader jpload = new JpanelLoader();
    String userMenu = String.valueOf(role_id);

    /**
     * Creates new form Main
     */
    public Main() {
        initComponents();
        Locale locale = new Locale("id","ID");
        Locale.setDefault(locale);
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setLocationRelativeTo(null);
        HomePanel home = new HomePanel();
        jpload.jPanelLoader(panelBody, home);
        label_name.setText(name);
        execute();
    }

    private void execute() {
        ImageIcon iconHome = new ImageIcon(getClass().getResource("/icon/tachometer.png"));
        ImageIcon icontransaction = new ImageIcon(getClass().getResource("/icon/transaction.png"));
        ImageIcon iconreport = new ImageIcon(getClass().getResource("/icon/report.png"));
        ImageIcon iconDatabase = new ImageIcon(getClass().getResource("/icon/database.png"));
        ImageIcon iconSubMenu = new ImageIcon(getClass().getResource("/icon/subMenu.png"));
        ImageIcon iconNext = new ImageIcon(getClass().getResource("/icon/next.png"));
        ImageIcon iconLogout = new ImageIcon(getClass().getResource("/icon/logout.png"));
        ImageIcon iconProfile = new ImageIcon(getClass().getResource("/icon/user.png"));
        ImageIcon iconKey = new ImageIcon(getClass().getResource("/icon/icon_key.png"));
        ImageIcon iconKeluar = new ImageIcon(getClass().getResource("/icon/checkout.png"));
        ImageIcon iconMasuk = new ImageIcon(getClass().getResource("/icon/checkin.png"));
        ImageIcon iconSetting = new ImageIcon(getClass().getResource("/icon/icon_setting.png"));
        ImageIcon iconHistori = new ImageIcon(getClass().getResource("/icon/icon_histori.png"));
        ImageIcon iconStok = new ImageIcon(getClass().getResource("/icon/icon_stock.png"));
        //menu home
        MenuItem menuHome = new MenuItem(iconHome, "Dashboard", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HomePanel home = new HomePanel();
                jpload.jPanelLoader(panelBody, home);
            }
        });
        //data produk
        MenuItem dataLoket = new MenuItem(iconSubMenu, "Loket", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataLoketPanel pro = new DataLoketPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        //data barang
        MenuItem dataBarang = new MenuItem(iconSubMenu, "Bahan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataBahanPanel pro = new DataBahanPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        //data barang
        MenuItem dataSuppliyer = new MenuItem(iconSubMenu, "Supplier", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataSuppliyerPanel pro = new DataSuppliyerPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        //data produk
        MenuItem dataProduk = new MenuItem(iconSubMenu, "Kemasan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataKemasanPanel pro = new DataKemasanPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        //menu database
        MenuItem master = new MenuItem(iconDatabase, "Master", null, dataLoket, dataBarang, dataProduk, dataSuppliyer);

        //barang masuk
        MenuItem produkItemMasuk = new MenuItem(iconSubMenu, "Produk Masuk", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PanelProdukMasuk pro = new PanelProdukMasuk();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem bahanItemMasuk = new MenuItem(iconSubMenu, "Bahan Masuk", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PanelBahanMasuk pro = new PanelBahanMasuk();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem barangMasuk = new MenuItem(iconMasuk, "Barang Masuk", null, produkItemMasuk, bahanItemMasuk);
        MenuItem produkItemKeluar = new MenuItem(iconSubMenu, "Produk Keluar", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PanelProdukKeluar pro = new PanelProdukKeluar();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem bahanItemKeluar = new MenuItem(iconSubMenu, "Bahan Keluar", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PanelBahanKeluar pro = new PanelBahanKeluar();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem barangKeluar = new MenuItem(iconKeluar, "Barang Keluar", null, produkItemKeluar, bahanItemKeluar);

        //report
        //report stok
        MenuItem reportStokBahan = new MenuItem(iconNext, "Stok Bahan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printStokBahan();
            }
        });
        MenuItem reportStokProduk = new MenuItem(iconNext, "Stok Produk", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printstokProduk();
            }
        });
        MenuItem laporanStok = new MenuItem(iconSubMenu, "Laporan Stok", null, reportStokBahan, reportStokProduk);

        //report transaksi masuk
        MenuItem reportBahanMasuk = new MenuItem(iconNext, "Bahan Masuk", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printBahanMasuk();
            }
        });
        MenuItem reportProdukMasuk = new MenuItem(iconNext, "Produk Masuk", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printProdukMasuk();
            }
        });
        MenuItem laporanTransksiMasuk = new MenuItem(iconSubMenu, "Laporan Transaksi Masuk", null, reportBahanMasuk, reportProdukMasuk);

        //report transaksi keluar
        MenuItem reportBahanKeluar = new MenuItem(iconNext, "Bahan Keluar", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printBahanKeluar();
            }
        });
        MenuItem reportProdukKeluar = new MenuItem(iconNext, "Produk Keluar", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printProdukKeluar();
            }
        });
        MenuItem laporanTransksiKeluar = new MenuItem(iconSubMenu, "Laporan Transaksi Keluar", null, reportBahanKeluar, reportProdukKeluar);
        //report data suppliyer
        MenuItem reportDataSuppliyer = new MenuItem(iconSubMenu, "Laporan Data Supplier", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                printDataSuppliyer();
            }
        });

        MenuItem laporan = new MenuItem(iconreport, "Laporan", null, laporanStok, laporanTransksiMasuk, laporanTransksiKeluar, reportDataSuppliyer);

        //histori
        MenuItem historiBahanMasuk = new MenuItem(iconNext, "Bahan Masuk", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HistoriBahanMasukPanel pro = new HistoriBahanMasukPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem historiBahanKeluar = new MenuItem(iconNext, "Bahan Keluar", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HistoriBahanKeluarPanel pro = new HistoriBahanKeluarPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem historiProdukKeluar = new MenuItem(iconNext, "Produk Keluar", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HistoriProdukKeluarPanel pro = new HistoriProdukKeluarPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem historiProdukMasuk = new MenuItem(iconNext, "Produk Masuk", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HistoriProdukMasukPanel pro = new HistoriProdukMasukPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem transaksiKeluar = new MenuItem(iconSubMenu, "Transaksi Keluar", null, historiBahanKeluar, historiProdukKeluar);
        MenuItem transaksiMasuk = new MenuItem(iconSubMenu, "Transaksi Masuk", null, historiBahanMasuk, historiProdukMasuk);
        MenuItem histori = new MenuItem(iconHistori, "Histori", null, transaksiKeluar, transaksiMasuk);

        //stok
        MenuItem stokBahan = new MenuItem(iconSubMenu, "Stok Bahan", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StokBahanPanel pro = new StokBahanPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem stokProduk = new MenuItem(iconSubMenu, "Stok Produk", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StokProdukPanel pro = new StokProdukPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        MenuItem stok = new MenuItem(iconStok, "Stok", null, stokBahan, stokProduk);

        //hak akses
        MenuItem hakAkses = new MenuItem(iconSubMenu, "Hak Akses", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DataPenggunaPanel pro = new DataPenggunaPanel();
                jpload.jPanelLoader(panelBody, pro);
            }
        });
        //ganti password
        MenuItem password = new MenuItem(iconSubMenu, "Ganti Password", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FormGantiPassword pass = new FormGantiPassword();
                pass.setVisible(true);
                pass.pack();
                pass.setLocationRelativeTo(null);
                pass.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            }
        });

        //pengaturan
        MenuItem pengaturan = new MenuItem(iconSetting, "Pengaturan", null, hakAkses, password);
        //pengaturan
        MenuItem pengaturan1 = new MenuItem(iconSetting, "Pengaturan", null, password);
        //pengaturan
        MenuItem menuLogout = new MenuItem(iconLogout, "Log Out", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ok = JOptionPane.showConfirmDialog(null, "Log Out?", "Message", 2, JOptionPane.YES_NO_OPTION);
                if (ok == 0) {
                    showMessage("Log out successfully!");
                    Login login = new Login();
                    login.setVisible(true);
                    login.pack();
                    login.setLocationRelativeTo(null);
                    login.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    dispose();
                }
            }
        });
        switch (userMenu) {
            case "1":
                addMenu(menuHome, master, barangMasuk, barangKeluar, laporan, histori, stok, pengaturan, menuLogout);
                break;
            case "2":
                addMenu(menuHome, barangMasuk, barangKeluar, laporan, histori, stok, pengaturan1, menuLogout);
                break;
            case "3":
                addMenu(menuHome, barangMasuk, barangKeluar, laporan, histori, pengaturan1, menuLogout);
                break;
        }

//        addMenu(menuHome, master, barangMasuk, barangKeluar, laporan, histori, stok, pengaturan, menuLogout);
    }

    private void addMenu(MenuItem... menu) {
        for (int i = 0; i < menu.length; i++) {
            menus.add(menu[i]);
            ArrayList<MenuItem> subMenu = menu[i].getSubMenu();
            for (MenuItem m : subMenu) {
                addMenu(m);
            }
        }
        menus.revalidate();
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    private void printDataSuppliyer() {
        try {
            String namaFile = "src\\report\\LaporanDataSuppliyer.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printBahanMasuk() {
        try {
            String namaFile = "src\\report\\LaporanBahanMasuk.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printBahanKeluar() {
        try {
            String namaFile = "src\\report\\LaporanBahanKeluar.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printProdukMasuk() {
        try {
            String namaFile = "src\\report\\LaporanProdukMasuk.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printProdukKeluar() {
        try {
            String namaFile = "src\\report\\LaporanProdukKeluar.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printStokBahan() {
        try {
            String namaFile = "src\\report\\LaporanStokBahan.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void printstokProduk() {
        try {
            String namaFile = "src\\report\\LaporanStokProduk.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelHeader = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        label_name = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        panelMenu = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        menus = new javax.swing.JPanel();
        panelBody = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panelHeader.setBackground(new java.awt.Color(73, 74, 72));
        panelHeader.setPreferredSize(new java.awt.Dimension(561, 50));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons.png"))); // NOI18N
        jLabel1.setText("INVENTORY");

        label_name.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        label_name.setForeground(new java.awt.Color(255, 255, 255));
        label_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        label_name.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/person.png"))); // NOI18N
        label_name.setText("Nama");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(45, 83, 237));
        jLabel2.setText("SYSTEM");

        javax.swing.GroupLayout panelHeaderLayout = new javax.swing.GroupLayout(panelHeader);
        panelHeader.setLayout(panelHeaderLayout);
        panelHeaderLayout.setHorizontalGroup(
            panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(5, 5, 5)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(350, 350, 350)
                .addComponent(label_name, javax.swing.GroupLayout.DEFAULT_SIZE, 256, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelHeaderLayout.setVerticalGroup(
            panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panelHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(panelHeaderLayout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addComponent(label_name, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(panelHeader, java.awt.BorderLayout.PAGE_START);

        panelMenu.setBackground(new java.awt.Color(239, 240, 237));
        panelMenu.setPreferredSize(new java.awt.Dimension(250, 384));

        jScrollPane1.setBorder(null);

        menus.setBackground(new java.awt.Color(255, 255, 255));
        menus.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        menus.setLayout(new javax.swing.BoxLayout(menus, javax.swing.BoxLayout.Y_AXIS));
        jScrollPane1.setViewportView(menus);

        javax.swing.GroupLayout panelMenuLayout = new javax.swing.GroupLayout(panelMenu);
        panelMenu.setLayout(panelMenuLayout);
        panelMenuLayout.setHorizontalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMenuLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );
        panelMenuLayout.setVerticalGroup(
            panelMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
        );

        getContentPane().add(panelMenu, java.awt.BorderLayout.LINE_START);

        panelBody.setBackground(new java.awt.Color(255, 255, 255));
        panelBody.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout panelBodyLayout = new javax.swing.GroupLayout(panelBody);
        panelBody.setLayout(panelBodyLayout);
        panelBodyLayout.setHorizontalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 636, Short.MAX_VALUE)
        );
        panelBodyLayout.setVerticalGroup(
            panelBodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 403, Short.MAX_VALUE)
        );

        getContentPane().add(panelBody, java.awt.BorderLayout.CENTER);

        setSize(new java.awt.Dimension(906, 496));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Main().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JLabel label_name;
    private javax.swing.JPanel menus;
    public static javax.swing.JPanel panelBody;
    private javax.swing.JPanel panelHeader;
    private javax.swing.JPanel panelMenu;
    // End of variables declaration//GEN-END:variables

}
