/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel;

import auth.User;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;
import table.TableLoket;

/**
 *
 * @author Admin
 */
public class DataKemasanPanel extends javax.swing.JPanel {

    public String loketID;
    String id = null;
    String id_loket = null;
    String nama = null;
    String jenis = null;
    String id_user = User.getId_user();
    PreparedStatement ps;
    ResultSet rs;
    private DefaultTableModel tabmode;
    DefaultTableCellRenderer renderer;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    /**
     * Creates new form DataBarangPanel
     */
    public DataKemasanPanel() {
        initComponents();
        aktif();
        kosong();
        autonumber();
        datatable();
    }

    private void aktif() {
        txt_loketID.setEditable(false);
        txt_id.setEditable(false);
        txt_nama.requestFocus(true);
        btn_simpan.setEnabled(true);
        btn_ubah.setEnabled(false);
        btn_hapus.setEnabled(false);
    }

    public void itemTerpilih() {
        TableLoket Dpw = new TableLoket();
        Dpw.loket = this;
        txt_loketID.setText(loketID);
    }

    private void kosong() {
        txt_loketID.setText("");
        txt_cari.setText("");
        txt_nama.setText("");
    }

    private void autonumber() {
        try {
            String query = "select MAX(RIGHT(id,8)) AS NO  from kemasan order by id desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    txt_id.setText("IDK10000001");
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    txt_id.setText("IDK" + no);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }

    }

    private void simpanData() {
        String query = "INSERT INTO `kemasan`(`id`, `loket_id`, `nama`, `jenis`, `user_id`) VALUES  (?,?,?,?,?)";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, txt_id.getText().trim());
            ps.setString(2, txt_loketID.getText().trim());
            ps.setString(3, txt_nama.getText().trim());
            ps.setString(4, jComboJenis.getSelectedItem().toString());
            ps.setString(5, id_user);
            ps.executeUpdate();
            showMessage("Data telah ditambahkan!");
            autonumber();
            kosong();
            aktif();
        } catch (Exception e) {
            showMessage("Data gagal ditambahkan! " + e);
        }
    }

    private void updateData() {
        String query = "UPDATE `kemasan` SET `loket_id`=?,`nama`=?,`jenis`=? WHERE `id`='" + txt_id.getText() + "'";
        try {
            ps = MyConnection.getConnection().prepareStatement(query);
            ps.setString(1, txt_loketID.getText().trim());
            ps.setString(2, txt_nama.getText().trim());
            ps.setString(3, jComboJenis.getSelectedItem().toString());

            ps.executeUpdate();
            showMessage("Data telah diubah!");
            autonumber();
            kosong();
            aktif();
        } catch (Exception e) {
            showMessage("Data gagal diubah! " + e);
        }
    }

    private void showMessage(String message) {
        JOptionPane.showMessageDialog(null, message);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table_kemasan = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txt_id = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_nama = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jComboJenis = new javax.swing.JComboBox<>();
        btn_simpan = new javax.swing.JButton();
        btn_ubah = new javax.swing.JButton();
        btn_batal = new javax.swing.JButton();
        btn_hapus = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txt_loketID = new javax.swing.JTextField();
        btn_cariLoket = new javax.swing.JButton();
        txt_cari = new javax.swing.JTextField();
        btn_cari = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setText("Data Kemasan");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Tabel Data Kemasan");

        table_kemasan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_kemasan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_kemasanMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(table_kemasan);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setText("ID Kemasan");

        jLabel4.setText("Nama");

        jLabel7.setText("Jenis Kemasan");

        jComboJenis.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Tropical Slim", "Hilo", "Nutrisari", "L-Men", "WRP", "W'dank" }));

        btn_simpan.setBackground(new java.awt.Color(43, 186, 165));
        btn_simpan.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_simpan.setForeground(new java.awt.Color(255, 255, 255));
        btn_simpan.setText("Simpan");
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        btn_ubah.setBackground(new java.awt.Color(0, 102, 204));
        btn_ubah.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_ubah.setForeground(new java.awt.Color(255, 255, 255));
        btn_ubah.setText("Ubah");
        btn_ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahActionPerformed(evt);
            }
        });

        btn_batal.setBackground(new java.awt.Color(255, 102, 102));
        btn_batal.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_batal.setForeground(new java.awt.Color(255, 255, 255));
        btn_batal.setText("Batal");
        btn_batal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_batalActionPerformed(evt);
            }
        });

        btn_hapus.setBackground(new java.awt.Color(255, 102, 102));
        btn_hapus.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_hapus.setForeground(new java.awt.Color(255, 255, 255));
        btn_hapus.setText("Hapus");
        btn_hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hapusActionPerformed(evt);
            }
        });

        jLabel5.setText("ID Loket");

        btn_cariLoket.setText("Cari");
        btn_cariLoket.setMaximumSize(new java.awt.Dimension(51, 20));
        btn_cariLoket.setMinimumSize(new java.awt.Dimension(51, 20));
        btn_cariLoket.setPreferredSize(new java.awt.Dimension(51, 20));
        btn_cariLoket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariLoketActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(8, 8, 8)
                        .addComponent(txt_loketID, javax.swing.GroupLayout.DEFAULT_SIZE, 86, Short.MAX_VALUE)
                        .addGap(10, 10, 10)
                        .addComponent(btn_cariLoket, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addGap(10, 10, 10)
                        .addComponent(txt_id, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addGap(10, 10, 10)
                        .addComponent(txt_nama, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                        .addComponent(jLabel7)
                        .addGap(14, 14, 14)
                        .addComponent(jComboJenis, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btn_simpan)
                        .addGap(8, 8, 8)
                        .addComponent(btn_ubah)
                        .addGap(8, 8, 8)
                        .addComponent(btn_batal)
                        .addGap(8, 8, 8)
                        .addComponent(btn_hapus)))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_id, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txt_nama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(jComboJenis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txt_loketID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cariLoket, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_hapus)
                    .addComponent(btn_batal)
                    .addComponent(btn_ubah)
                    .addComponent(btn_simpan))
                .addGap(10, 10, 10))
        );

        txt_cari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_cariKeyReleased(evt);
            }
        });

        btn_cari.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cari.setText("Cari");
        btn_cari.setMaximumSize(new java.awt.Dimension(55, 20));
        btn_cari.setMinimumSize(new java.awt.Dimension(55, 20));
        btn_cari.setPreferredSize(new java.awt.Dimension(55, 20));
        btn_cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void table_kemasanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_kemasanMouseClicked
        // TODO add your handling code here:
        int selectedRow = table_kemasan.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) table_kemasan.getModel();
        txt_id.setText(model.getValueAt(selectedRow, 1).toString());
        txt_loketID.setText(model.getValueAt(selectedRow, 2).toString());
        txt_nama.setText(model.getValueAt(selectedRow, 3).toString());
        jComboJenis.setSelectedItem(model.getValueAt(selectedRow, 4).toString());
//        txt_qty.setText(model.getValueAt(selectedRow, 4).toString());
//        tanggal = model.getValueAt(selectedRow, 5).toString();
//        try {
//            java.util.Date d = dateFormat.parse(tanggal);
//            txt_tanggal.setDate(d);
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
        btn_simpan.setEnabled(false);
        btn_ubah.setEnabled(true);
        btn_hapus.setEnabled(true);
    }//GEN-LAST:event_table_kemasanMouseClicked

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        // TODO add your handling code here:
        nama = txt_nama.getText().trim();
        id_loket = txt_loketID.getText().trim();
        jenis = jComboJenis.getSelectedItem().toString();
        if (id_loket.isEmpty()) {
            showMessage("Loket tidak boleh kosong!");
            btn_cariLoket.requestFocus();
        } else if (nama.isEmpty()) {
            showMessage("Nama tidak boleh kosong!");
            txt_nama.requestFocus();
        } else {
            simpanData();
            datatable();
        }
    }//GEN-LAST:event_btn_simpanActionPerformed

    private void btn_batalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_batalActionPerformed
        // TODO add your handling code here:
        kosong();
        autonumber();
        aktif();
    }//GEN-LAST:event_btn_batalActionPerformed

    private void btn_ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahActionPerformed
        // TODO add your handling code here:
        id_loket = txt_loketID.getText().trim();
        nama = txt_nama.getText().trim();
        jenis = jComboJenis.getSelectedItem().toString();
        
        if (id_loket.isEmpty()) {
            showMessage("Loket tidak boleh kosong!");
            btn_cariLoket.requestFocus();
        } else if (nama.isEmpty()) {
            showMessage("Nama tidak boleh kosong!");
            txt_nama.requestFocus();
        } else {
            updateData();
            datatable();
        }
    }//GEN-LAST:event_btn_ubahActionPerformed

    private void btn_hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hapusActionPerformed
        // TODO add your handling code here:
        int ok = JOptionPane.showConfirmDialog(null, "hapus", "konfirmasi dialog", JOptionPane.YES_NO_OPTION);
        if (ok == 0) {
            int in = table_kemasan.getSelectedRowCount();
            if (in == 0) {
                showMessage("Mohon pilih data di tabel terlebih dahulu!");
            } else {
                try {
                    String query = "DELETE FROM `kemasan` WHERE `id`='" + txt_id.getText() + "'";
                    PreparedStatement stat = MyConnection.getConnection().prepareStatement(query);
                    stat.executeUpdate();
                    showMessage("Data berhasil dihapus");
                    kosong();
                    autonumber();
                    datatable();
                    aktif();
                } catch (SQLException e) {
                    showMessage("Data gagal dihapus" + e);
                }
            }
        }
    }//GEN-LAST:event_btn_hapusActionPerformed

    private void txt_cariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_cariKeyReleased
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_txt_cariKeyReleased

    private void btn_cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariActionPerformed
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_btn_cariActionPerformed

    private void btn_cariLoketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariLoketActionPerformed
        // TODO add your handling code here:
        TableLoket pro = new TableLoket();
        pro.loket = this;
        pro.jLabelTitle.setText("Data Loket Produk");
        pro.pack();
        pro.setVisible(true);
        pro.setLocationRelativeTo(null);
        pro.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_btn_cariLoketActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_batal;
    private javax.swing.JButton btn_cari;
    private javax.swing.JButton btn_cariLoket;
    private javax.swing.JButton btn_hapus;
    private javax.swing.JButton btn_simpan;
    private javax.swing.JButton btn_ubah;
    private javax.swing.JComboBox<String> jComboJenis;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable table_kemasan;
    private javax.swing.JTextField txt_cari;
    private javax.swing.JTextField txt_id;
    private javax.swing.JTextField txt_loketID;
    private javax.swing.JTextField txt_nama;
    // End of variables declaration//GEN-END:variables

    private void datatable() {
        String[] Header = {"No.", "ID Kemasan","ID Loket", "Nama", "Jenis"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) table_kemasan.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        String cariitem = txt_cari.getText();
        try {
            String query = "SELECT `id`,`loket_id`, `nama`, `jenis` FROM `kemasan` WHERE `id` like '%" + cariitem + "%' or `nama` like '%" + cariitem + "%' order by `id` desc";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            int no = 1;
            while (rs.next()) {
                tabmode.addRow(new Object[]{
                    Integer.toString(no),
                    rs.getString(1),
                    rs.getString(2),
                    rs.getString(3),
                        rs.getString(4)
                });
                no++;
            }
            table_kemasan.setModel(tabmode);
            table_kemasan.getColumnModel().getColumn(0).setPreferredWidth(10);
            table_kemasan.getColumnModel().getColumn(1).setPreferredWidth(100);
            table_kemasan.getColumnModel().getColumn(2).setPreferredWidth(100);
            table_kemasan.getColumnModel().getColumn(3).setPreferredWidth(300);
            table_kemasan.getColumnModel().getColumn(3).setPreferredWidth(150);
            table_kemasan.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "data gagal dipanggil" + e);
        }
    }
}
