/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel.histori;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;
import panel.transaksi.*;

/**
 *
 * @author Admin
 */
public class HistoriBahanMasukPanel extends javax.swing.JPanel {

    PreparedStatement ps;
    ResultSet rs;
    DefaultTableCellRenderer renderer;
    private DefaultTableModel tabmode;

    /**
     * Creates new form PanelBahanMasuk
     */
    public HistoriBahanMasukPanel() {
        initComponents();
        datatable();
        jumlah();
    }

    private void jumlah() {
        tabmode = (DefaultTableModel) tabel_histori.getModel();
        int d = 0;
        for (int e = 0; e < tabmode.getRowCount(); e++) {
            d += Integer.parseInt(tabmode.getValueAt(e, 8).toString());
        }
        labelTotal.setText("" + d);
    }

    private void datatable() {
        String[] Header = {"No.", "Waktu", "ID Transaksi", "Suppliyer", "No. PO", "Kode Bahan", "Nama Bahan", "Jenis Bahan", "Qty"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) tabel_histori.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        String cariitem = txt_cari.getText();
        try {
            String query = "SELECT\n"
                    + "     transaksi_bahan_m.`id` AS transaksi_bahan_m_id,\n"
                    + "     transaksi_bahan_m.`id_transaksi` AS transaksi_bahan_m_id_transaksi,\n"
                    + "     transaksi_bahan_m.`supliyer_id` AS transaksi_bahan_m_supliyer_id,\n"
                    + "     transaksi_bahan_m.`no_po` AS transaksi_bahan_m_no_po,\n"
                    + "     transaksi_bahan_m.`kode_bahan` AS transaksi_bahan_m_kode_bahan,\n"
                    + "     transaksi_bahan_m.`nama_bahan` AS transaksi_bahan_m_nama_bahan,\n"
                    + "     transaksi_bahan_m.`qty` AS transaksi_bahan_m_qty,\n"
                    + "     transaksi_bahan_m.`created_date` AS transaksi_bahan_m_created_date,\n"
                    + "     bahan.`kode` AS bahan_kode,\n"
                    + "     bahan.`nama` AS bahan_nama,\n"
                    + "     bahan.`jenis` AS bahan_jenis,\n"
                    + "     suppliyer.`id` AS suppliyer_id,\n"
                    + "     suppliyer.`name` AS suppliyer_name,\n"
                    + "     suppliyer.`phone` AS suppliyer_phone,\n"
                    + "     suppliyer.`alamat` AS suppliyer_alamat\n"
                    + "FROM\n"
                    + "     `bahan` bahan INNER JOIN `transaksi_bahan_m` transaksi_bahan_m ON bahan.`kode` = transaksi_bahan_m.`kode_bahan`\n"
                    + "     INNER JOIN `suppliyer` suppliyer ON transaksi_bahan_m.`supliyer_id` = suppliyer.`id`"
                    + "WHERE transaksi_bahan_m.`id_transaksi` like '%" + cariitem + "%'"
                    + "OR bahan.`nama` like '%" + cariitem + "%'"
                    + " ORDER By transaksi_bahan_m.`created_date` DESC";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            int no = 1;
            while (rs.next()) {
                Object[] obj = new Object[9];
                obj[0] = Integer.toString(no);
                obj[1] = rs.getString("transaksi_bahan_m_created_date");
                obj[2] = rs.getString("transaksi_bahan_m_id_transaksi");
                obj[3] = rs.getString("suppliyer_name");
                obj[4] = rs.getString("transaksi_bahan_m_no_po");
                obj[5] = rs.getString("transaksi_bahan_m_kode_bahan");
                obj[6] = rs.getString("transaksi_bahan_m_nama_bahan");
                obj[7] = rs.getString("bahan_jenis");
                obj[8] = rs.getString("transaksi_bahan_m_qty");
                tabmode.addRow(obj);

                no++;
            }
            tabel_histori.setModel(tabmode);
            tabel_histori.getColumnModel().getColumn(0).setPreferredWidth(10);
            tabel_histori.getColumnModel().getColumn(1).setPreferredWidth(110);
            tabel_histori.getColumnModel().getColumn(2).setPreferredWidth(90);
            tabel_histori.getColumnModel().getColumn(3).setPreferredWidth(120);
            tabel_histori.getColumnModel().getColumn(4).setPreferredWidth(100);
            tabel_histori.getColumnModel().getColumn(5).setPreferredWidth(90);
            tabel_histori.getColumnModel().getColumn(6).setPreferredWidth(120);
            tabel_histori.getColumnModel().getColumn(7).setPreferredWidth(100);
            tabel_histori.getColumnModel().getColumn(8).setPreferredWidth(50);
            tabel_histori.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Failed to call data" + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabel_histori = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        txt_cari = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        labelTotal = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Histori Bahan Masuk");

        tabel_histori.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabel_histori);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Tabel Histori Bahan Masuk");

        jButton1.setText("Cari");
        jButton1.setMaximumSize(new java.awt.Dimension(51, 20));
        jButton1.setMinimumSize(new java.awt.Dimension(51, 20));
        jButton1.setPreferredSize(new java.awt.Dimension(51, 20));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        txt_cari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_cariKeyReleased(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Total :");

        labelTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        labelTotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTotal.setText("0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 415, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addGap(5, 5, 5)
                        .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txt_cariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_cariKeyReleased
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_txt_cariKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JTable tabel_histori;
    private javax.swing.JTextField txt_cari;
    // End of variables declaration//GEN-END:variables
}
