/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel.histori;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;
import panel.transaksi.*;

/**
 *
 * @author Admin
 */
public class HistoriProdukKeluarPanel extends javax.swing.JPanel {

    PreparedStatement ps;
    ResultSet rs;
    DefaultTableCellRenderer renderer;
    private DefaultTableModel tabmode;

    /**
     * Creates new form PanelBahanMasuk
     */
    public HistoriProdukKeluarPanel() {
        initComponents();
        datatable();
        jumlah();
    }

    private void jumlah() {
        tabmode = (DefaultTableModel) tabel_histori.getModel();
        int d = 0;
        for (int e = 0; e < tabmode.getRowCount(); e++) {
            d += Integer.parseInt(tabmode.getValueAt(e, 7).toString());
        }
        labelTotal.setText("" + d);
    }

    private void datatable() {
        String[] Header = {"No.", "Waktu", "ID Transaksi", "Staff", "Kode Produk", "Nama Produk", "Jenis Produk", "Qty"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) tabel_histori.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        String cariitem = txt_cari.getText();
        try {
            String query = "SELECT\n"
                    + "     transaksi_produk_k.`id` AS transaksi_produk_k_id,\n"
                    + "     transaksi_produk_k.`id_transaksi` AS transaksi_produk_k_id_transaksi,\n"
                    + "     transaksi_produk_k.`produk_id` AS transaksi_produk_k_produk_id,\n"
                    + "     transaksi_produk_k.`nama_produk` AS transaksi_produk_k_nama_produk,\n"
                    + "     transaksi_produk_k.`qty` AS transaksi_produk_k_qty,\n"
                    + "     transaksi_produk_k.`user_id` AS transaksi_produk_k_user_id,\n"
                    + "     transaksi_produk_k.`created_date` AS transaksi_produk_k_created_date,\n"
                    + "     kemasan.`id` AS kemasan_id,\n"
                    + "     kemasan.`nama` AS kemasan_nama,\n"
                    + "     kemasan.`jenis` AS kemasan_jenis,\n"
                    + "     kemasan.`user_id` AS kemasan_user_id,\n"
                    + "     kemasan.`created_at` AS kemasan_created_at,\n"
                    + "     users.`id_user` AS users_id_user,\n"
                    + "     users.`name` AS users_name,\n"
                    + "     users.`username` AS users_username,\n"
                    + "     users.`password` AS users_password,\n"
                    + "     users.`created_at` AS users_created_at,\n"
                    + "     users.`is_active` AS users_is_active,\n"
                    + "     users.`role_id` AS users_role_id\n"
                    + "FROM\n"
                    + "     `transaksi_produk_k` transaksi_produk_k INNER JOIN `users` users ON transaksi_produk_k.`user_id` = users.`id_user`\n"
                    + "     INNER JOIN `kemasan` kemasan ON users.`id_user` = kemasan.`user_id`\n"
                    + "     AND transaksi_produk_k.`produk_id` = kemasan.`id`\n"
                    + "WHERE transaksi_produk_k.`id_transaksi` like '%" + cariitem + "%'"
                    + "OR kemasan.`nama` like '%" + cariitem + "%'"
                    + " ORDER By transaksi_produk_k.`created_date` DESC";
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            int no = 1;
            while (rs.next()) {
                Object[] obj = new Object[8];
                obj[0] = Integer.toString(no);
                obj[1] = rs.getString("transaksi_produk_k_created_date");
                obj[2] = rs.getString("transaksi_produk_k_id_transaksi");
                obj[3] = rs.getString("users_name");
                obj[4] = rs.getString("transaksi_produk_k_produk_id");
                obj[5] = rs.getString("kemasan_nama");
                obj[6] = rs.getString("kemasan_jenis");
                obj[7] = rs.getString("transaksi_produk_k_qty");
                tabmode.addRow(obj);

                no++;
            }
            tabel_histori.setModel(tabmode);
            tabel_histori.getColumnModel().getColumn(0).setPreferredWidth(10);
            tabel_histori.getColumnModel().getColumn(1).setPreferredWidth(110);
            tabel_histori.getColumnModel().getColumn(2).setPreferredWidth(90);
            tabel_histori.getColumnModel().getColumn(3).setPreferredWidth(120);
            tabel_histori.getColumnModel().getColumn(4).setPreferredWidth(100);
            tabel_histori.getColumnModel().getColumn(5).setPreferredWidth(150);
            tabel_histori.getColumnModel().getColumn(6).setPreferredWidth(120);
            tabel_histori.getColumnModel().getColumn(7).setPreferredWidth(50);
            tabel_histori.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Failed to call data" + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabel_histori = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        btn_cari = new javax.swing.JButton();
        txt_cari = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        labelTotal = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Histori Produk Keluar");

        tabel_histori.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabel_histori);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Tabel Histori Produk Keluar");

        btn_cari.setText("Cari");
        btn_cari.setMaximumSize(new java.awt.Dimension(51, 20));
        btn_cari.setMinimumSize(new java.awt.Dimension(51, 20));
        btn_cari.setPreferredSize(new java.awt.Dimension(51, 20));
        btn_cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariActionPerformed(evt);
            }
        });

        txt_cari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_cariKeyReleased(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel3.setText("Total :");

        labelTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        labelTotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTotal.setText("0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 427, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addGap(5, 5, 5)
                        .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addComponent(jScrollPane1)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btn_cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariActionPerformed
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_btn_cariActionPerformed

    private void txt_cariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_cariKeyReleased
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_txt_cariKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cari;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JTable tabel_histori;
    private javax.swing.JTextField txt_cari;
    // End of variables declaration//GEN-END:variables
}
