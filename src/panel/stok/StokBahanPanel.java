/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel.stok;

import java.awt.Container;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;
import panel.histori.*;
import panel.transaksi.*;

/**
 *
 * @author Admin
 */
public class StokBahanPanel extends javax.swing.JPanel {

    PreparedStatement ps;
    ResultSet rs;
    private DefaultTableModel tabmode;
    DefaultTableCellRenderer renderer;

    /**
     * Creates new form PanelBahanMasuk
     */
    public StokBahanPanel() {
        initComponents();
        datatable();
        bahanMasuk();
        bahanKeluar();
        bahanSisa();
    }

    private void bahanMasuk() {
        tabmode = (DefaultTableModel) tabel_bahan.getModel();
        int d = 0;
        for (int e = 0; e < tabmode.getRowCount(); e++) {
            d += Integer.parseInt(tabmode.getValueAt(e, 6).toString());
        }
        labelMasuk.setText("" + d);
    }

    private void bahanKeluar() {
        tabmode = (DefaultTableModel) tabel_bahan.getModel();
        int d = 0;
        for (int e = 0; e < tabmode.getRowCount(); e++) {
            d += Integer.parseInt(tabmode.getValueAt(e, 7).toString());
        }
        labelKeluar.setText("" + d);
    }

    private void bahanSisa() {
        tabmode = (DefaultTableModel) tabel_bahan.getModel();
        int d = 0;
        for (int e = 0; e < tabmode.getRowCount(); e++) {
            d += Integer.parseInt(tabmode.getValueAt(e, 8).toString());
        }
        labelSisa.setText("" + d);
    }

    private void datatable() {
        String[] Header = {"No.", "Kode Bahan", "Nama Bahan", "Jenis", "No. Loket", "Nama Loket", "Bahan Masuk", "Bahan Keluar", "Sisa Bahan"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) tabel_bahan.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        String cariitem = txt_cari.getText();
        try {
            String query = "SELECT\n"
                    + "     bahan.`kode` AS bahan_kode,\n"
                    + "     bahan.`nama` AS bahan_nama,\n"
                    + "     bahan.`jenis` AS bahan_jenis,\n"
                    + "     bahan.`loket_id` AS bahan_loket_id,\n"
                    + "     transaksi_bahan_k_id,\n"
                    + "     transaksi_bahan_k_id_transaksi,\n"
                    + "     transaksi_bahan_k_kode_bahan,\n"
                    + "     transaksi_bahan_k_nama_bahan,\n"
                    + "     transaksi_bahan_k_qty,\n"
                    + "     transaksi_bahan_m_id_transaksi,\n"
                    + "     transaksi_bahan_m_no_po,\n"
                    + "     transaksi_bahan_m_kode_bahan,\n"
                    + "     transaksi_bahan_m_nama_bahan,\n"
                    + "     transaksi_bahan_m_qty,\n"
                    + "     loket.`id` AS loket_id,\n"
                    + "     loket.`nomor` AS loket_nomor,\n"
                    + "     loket.`nama` AS loket_nama,\n"
                    + "     loket.`lokasi` AS loket_lokasi,\n"
                    + "     loket.`kapasitas` AS loket_kapasitas\n"
                    + "FROM\n"
                    + "     `bahan` bahan INNER JOIN `loket` loket ON bahan.`loket_id` = loket.`id`\n"
                    + "     LEFT JOIN(\n"
                    + "SELECT\n"
                    + "     `id` AS transaksi_bahan_k_id,\n"
                    + "     `id_transaksi` AS transaksi_bahan_k_id_transaksi,\n"
                    + "     `kode_bahan` AS transaksi_bahan_k_kode_bahan,\n"
                    + "     `nama_bahan` AS transaksi_bahan_k_nama_bahan,\n"
                    + "sum(`qty`) AS transaksi_bahan_k_qty\n"
                    + "FROM\n"
                    + "`transaksi_bahan_k`\n"
                    + "GROUP BY kode_bahan\n"
                    + ")\n"
                    + "transaksi_bahan_k ON bahan.`kode` = transaksi_bahan_k_kode_bahan\n"
                    + "LEFT JOIN(\n"
                    + "SELECT\n"
                    + "     `id_transaksi` AS transaksi_bahan_m_id_transaksi,\n"
                    + "     `no_po` AS transaksi_bahan_m_no_po,\n"
                    + "     `kode_bahan` AS transaksi_bahan_m_kode_bahan,\n"
                    + "     `nama_bahan` AS transaksi_bahan_m_nama_bahan,\n"
                    + "     sum(`qty`) AS transaksi_bahan_m_qty\n"
                    + "FROM\n"
                    + "`transaksi_bahan_m`\n"
                    + "GROUP BY kode_bahan\n"
                    + ")\n"
                    + "transaksi_bahan_m ON bahan.`kode` = transaksi_bahan_m_kode_bahan\n"
                    + "WHERE bahan.`nama` like '%" + cariitem + "%'"
                    + "GROUP BY bahan.`kode`";

            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            int no = 1;
            while (rs.next()) {
                Object[] obj = new Object[9];
                obj[0] = Integer.toString(no);
                obj[1] = rs.getString("bahan_kode");
                obj[2] = rs.getString("bahan_nama");
                obj[3] = rs.getString("bahan_jenis");
                obj[4] = rs.getString("loket_nomor");
                obj[5] = rs.getString("loket_nama");
                int kapasitas = rs.getInt("loket_kapasitas");
                int bahan_m = rs.getInt("transaksi_bahan_m_qty");
                int bahan_k = rs.getInt("transaksi_bahan_k_qty");
                obj[6] = bahan_m;
                obj[7] = bahan_k;
                int sisaBahan = bahan_m - bahan_k;
                obj[8] = sisaBahan;
                tabmode.addRow(obj);

                no++;
            }
            tabel_bahan.setModel(tabmode);
            tabel_bahan.getColumnModel().getColumn(0).setPreferredWidth(30);
            tabel_bahan.getColumnModel().getColumn(1).setPreferredWidth(100);
            tabel_bahan.getColumnModel().getColumn(2).setPreferredWidth(200);
            tabel_bahan.getColumnModel().getColumn(3).setPreferredWidth(150);
            tabel_bahan.getColumnModel().getColumn(4).setPreferredWidth(100);
            tabel_bahan.getColumnModel().getColumn(5).setPreferredWidth(150);
            tabel_bahan.getColumnModel().getColumn(6).setPreferredWidth(100);
            tabel_bahan.getColumnModel().getColumn(7).setPreferredWidth(100);
            tabel_bahan.getColumnModel().getColumn(8).setPreferredWidth(100);
            tabel_bahan.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Failed to call data" + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabel_bahan = new javax.swing.JTable();
        btn_cari = new javax.swing.JButton();
        txt_cari = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        labelMasuk = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        labelKeluar = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        labelSisa = new javax.swing.JLabel();
        btn_cetak = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Stok Bahan");

        tabel_bahan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tabel_bahan);

        btn_cari.setText("Cari");
        btn_cari.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_cari.setMaximumSize(new java.awt.Dimension(51, 20));
        btn_cari.setMinimumSize(new java.awt.Dimension(51, 20));
        btn_cari.setPreferredSize(new java.awt.Dimension(51, 20));
        btn_cari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariActionPerformed(evt);
            }
        });

        txt_cari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_cariKeyReleased(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Bahan Masuk");

        labelMasuk.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelMasuk.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelMasuk.setText("jLabel6");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelMasuk, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel3)
                .addGap(6, 6, 6)
                .addComponent(labelMasuk, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Bahan Keluar");

        labelKeluar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelKeluar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelKeluar.setText("jLabel6");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)
                    .addComponent(labelKeluar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel4)
                .addGap(6, 6, 6)
                .addComponent(labelKeluar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Sisa Bahan");

        labelSisa.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        labelSisa.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelSisa.setText("jLabel6");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 104, Short.MAX_VALUE)
                    .addComponent(labelSisa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel5)
                .addGap(6, 6, 6)
                .addComponent(labelSisa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(8, 8, 8))
        );

        btn_cetak.setBackground(new java.awt.Color(255, 255, 255));
        btn_cetak.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/icons8_print_16px.png"))); // NOI18N
        btn_cetak.setText("Cetak");
        btn_cetak.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_cetak.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cetakActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                        .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(btn_cetak))
                            .addComponent(jScrollPane1))
                        .addGap(10, 10, 10))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cetak))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txt_cariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_cariKeyReleased
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_txt_cariKeyReleased

    private void btn_cariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariActionPerformed
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_btn_cariActionPerformed

    private void btn_cetakActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cetakActionPerformed
        // TODO add your handling code here:
        try {
            String namaFile = "src\\report\\LaporanStokBahan.jasper";
            HashMap param = new HashMap();
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());

        }
    }//GEN-LAST:event_btn_cetakActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cari;
    private javax.swing.JButton btn_cetak;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel labelKeluar;
    private javax.swing.JLabel labelMasuk;
    private javax.swing.JLabel labelSisa;
    private javax.swing.JTable tabel_bahan;
    private javax.swing.JTextField txt_cari;
    // End of variables declaration//GEN-END:variables
}
