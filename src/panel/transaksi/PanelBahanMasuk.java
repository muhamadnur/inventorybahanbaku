/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package panel.transaksi;

import auth.User;
import java.awt.Container;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.swing.JRViewer;
import net.sf.jasperreports.view.JasperViewer;
import table.TableBahan;
import table.TableSuppliyer;

/**
 *
 * @author Admin
 */
public class PanelBahanMasuk extends javax.swing.JPanel {

    String id_user = User.getId_user();
    PreparedStatement ps;
    ResultSet rs;
    public int sisaLoket;
    public String suppliyerID;
    public String produkID;
    public String namaProduk;
    DefaultTableCellRenderer renderer;
    private DefaultTableModel tabmode;
    String kode = null;
    String id_suppliyer = null;
    String nomer_po = null;
    String nama = null;
    String qty = null;
    int sisa;

    /**
     * Creates new form PanelBahanMasuk
     */
    public PanelBahanMasuk() {
        initComponents();
        datatable();
        aktif();
        kosong();
        autonumber();
    }

    private void printData() {
        try {
            String namaFile = "src\\report\\BahanMasuk.jasper";
            HashMap param = new HashMap();
            param.put("ID_Transaksi", txt_idTransaksi.getText());
            MyConnection d = new MyConnection();
            Connection con = d.getConnection();
            JasperPrint print = JasperFillManager.fillReport(namaFile, param, con);
            JasperViewer.viewReport(print, false);
            JasperViewer.setDefaultLookAndFeelDecorated(true);
            JRViewer viewer = new JRViewer(print);
            Container c = getRootPane();
            c.add(viewer);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }

    private void autonumber() {
        try {
            String query = "select MAX(RIGHT(id_transaksi,8)) AS NO  from transaksi_bahan_m order by id_transaksi desc";;
            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            while (rs.next()) {
                if (rs.first() == false) {
                    txt_idTransaksi.setText("IBM00000001");
                } else {
                    rs.last();
                    int auto_id = rs.getInt(1) + 1;
                    String no = String.valueOf(auto_id);
                    int Nomor = no.length();
                    //MENGATUR jumlah 0
                    for (int j = 0; j < 8 - Nomor; j++) {
                        no = "0" + no;
                    }
                    txt_idTransaksi.setText("IBM" + no);
                }
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();//penanganan masalah
        }
    }

    private void datatable() {
        String[] Header = {"ID Suppliyer", "No. PO", "Kode Bahan", "Nama Bahan", "Qty"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) tabel_bahanMasuk.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        tabel_bahanMasuk.setModel(tabmode);
        tabel_bahanMasuk.getColumnModel().getColumn(0).setPreferredWidth(100);
        tabel_bahanMasuk.getColumnModel().getColumn(1).setPreferredWidth(100);
        tabel_bahanMasuk.getColumnModel().getColumn(2).setPreferredWidth(100);
        tabel_bahanMasuk.getColumnModel().getColumn(3).setPreferredWidth(200);
        tabel_bahanMasuk.getColumnModel().getColumn(4).setPreferredWidth(100);
        tabel_bahanMasuk.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
    }

    private void aktif() {
        btn_tambah.setEnabled(true);
        btn_simpan.setEnabled(true);
        btn_hapus.setEnabled(false);
        btn_ubah.setEnabled(false);
        txt_idTransaksi.setEditable(false);
        txt_idSuppliyer.setEditable(false);
        txt_kodeBahan.setEditable(false);
        txt_namaBahan.setEditable(false);
        txt_qty.requestFocus();
    }

    private void kosong() {
        txt_kodeBahan.setText("");
        txt_nomerPO.setText("");
        txt_namaBahan.setText("");
        txt_qty.setText("");
    }

    public void itemTerpilihSuppliyer() {
        TableSuppliyer suppliyer = new TableSuppliyer();
        suppliyer.bahanMasuk = this;
        txt_idSuppliyer.setText(suppliyerID);
    }

    public void itemTerpilihBahan() {
        TableBahan bahan = new TableBahan();
        bahan.bahanMasuk = this;
        txt_kodeBahan.setText(produkID);
        txt_namaBahan.setText(namaProduk);
        sisa = sisaLoket;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabel_bahanMasuk = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txt_idSuppliyer = new javax.swing.JTextField();
        btn_cariSuppliyer = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txt_nomerPO = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txt_kodeBahan = new javax.swing.JTextField();
        btn_cariKode = new javax.swing.JButton();
        txt_namaBahan = new javax.swing.JTextField();
        txt_qty = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_idTransaksi = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        labelTotal = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btn_tambah = new javax.swing.JButton();
        btn_ubah = new javax.swing.JButton();
        btn_hapus = new javax.swing.JButton();
        btn_simpan = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Bahan Masuk");

        tabel_bahanMasuk.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tabel_bahanMasuk.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabel_bahanMasukMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabel_bahanMasuk);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setText("Tabel Bahan Masuk");

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel3.setText("ID Suppliyer");

        btn_cariSuppliyer.setText("Cari");
        btn_cariSuppliyer.setMaximumSize(new java.awt.Dimension(51, 20));
        btn_cariSuppliyer.setMinimumSize(new java.awt.Dimension(51, 20));
        btn_cariSuppliyer.setPreferredSize(new java.awt.Dimension(51, 20));
        btn_cariSuppliyer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariSuppliyerActionPerformed(evt);
            }
        });

        jLabel4.setText("No. PO");

        jLabel5.setText("Kode Bahan");

        jLabel6.setText("Nama Bahan");

        jLabel7.setText("Qty");

        btn_cariKode.setText("Cari");
        btn_cariKode.setMaximumSize(new java.awt.Dimension(51, 20));
        btn_cariKode.setMinimumSize(new java.awt.Dimension(51, 20));
        btn_cariKode.setPreferredSize(new java.awt.Dimension(51, 20));
        btn_cariKode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cariKodeActionPerformed(evt);
            }
        });

        jLabel10.setText("ID Transaksi");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txt_idSuppliyer, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(btn_cariSuppliyer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(txt_nomerPO)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txt_kodeBahan, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(btn_cariKode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(txt_qty)
                    .addComponent(txt_namaBahan)
                    .addComponent(txt_idTransaksi))
                .addGap(10, 10, 10))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txt_idTransaksi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txt_idSuppliyer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cariSuppliyer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txt_nomerPO, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txt_kodeBahan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cariKode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txt_namaBahan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txt_qty, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Total");

        labelTotal.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        labelTotal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTotal.setText("0");

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        btn_tambah.setBackground(new java.awt.Color(43, 186, 165));
        btn_tambah.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_tambah.setText("Tambah");
        btn_tambah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_tambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_tambahActionPerformed(evt);
            }
        });

        btn_ubah.setBackground(new java.awt.Color(255, 255, 51));
        btn_ubah.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_ubah.setText("Ubah");
        btn_ubah.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_ubah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ubahActionPerformed(evt);
            }
        });

        btn_hapus.setBackground(new java.awt.Color(255, 102, 102));
        btn_hapus.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_hapus.setForeground(new java.awt.Color(255, 255, 255));
        btn_hapus.setText("Hapus");
        btn_hapus.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_hapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_hapusActionPerformed(evt);
            }
        });

        btn_simpan.setBackground(new java.awt.Color(0, 102, 204));
        btn_simpan.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_simpan.setForeground(new java.awt.Color(255, 255, 255));
        btn_simpan.setText("Simpan");
        btn_simpan.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_simpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_simpanActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(65, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_simpan, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_hapus, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_ubah, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_tambah, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(btn_tambah)
                .addGap(10, 10, 10)
                .addComponent(btn_ubah)
                .addGap(10, 10, 10)
                .addComponent(btn_hapus)
                .addGap(10, 10, 10)
                .addComponent(btn_simpan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel1)
                        .addGap(721, 721, 721)
                        .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 144, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jScrollPane1))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(labelTotal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(jLabel2)
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1))
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btn_cariKodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariKodeActionPerformed
        // TODO add your handling code here:
        TableBahan pro = new TableBahan();
        pro.bahanMasuk = this;
        pro.jLabelTitle.setText("Data Bahan Masuk");
        pro.pack();
        pro.setVisible(true);
        pro.setLocationRelativeTo(null);
        pro.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_btn_cariKodeActionPerformed

    private void btn_cariSuppliyerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cariSuppliyerActionPerformed
        // TODO add your handling code here:
        TableSuppliyer pro = new TableSuppliyer();
        pro.bahanMasuk = this;
        pro.pack();
        pro.setVisible(true);
        pro.setLocationRelativeTo(null);
        pro.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }//GEN-LAST:event_btn_cariSuppliyerActionPerformed

    private void btn_tambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_tambahActionPerformed
        // TODO add your handling code here:
        id_suppliyer = txt_idSuppliyer.getText();
        nomer_po = txt_nomerPO.getText().trim();
        kode = txt_kodeBahan.getText().trim();
        nama = txt_namaBahan.getText().trim();
        qty = txt_qty.getText().trim();
        int kapasitas = Integer.parseInt(qty);
        int total = sisa - kapasitas;

        if (id_suppliyer.isEmpty()) {
            JOptionPane.showMessageDialog(null, "ID Suppliyer tidak boleh kosong!");
            btn_cariSuppliyer.requestFocus();
        } else if (nomer_po.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nomer PO tidak boleh kosong!");
            txt_nomerPO.requestFocus();
        } else if (kode.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Kode tidak boleh kosong!");
            txt_kodeBahan.requestFocus();
        } else if (nama.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nama tidak boleh kosong!");
        } else if (qty.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Qty tidak boleh kosong!");
            txt_qty.requestFocus();
        } else {
            if (total < 0) {
                JOptionPane.showMessageDialog(null, "Qty melebihi sisa kapasitas loket!");
            } else {
                int ok = JOptionPane.showConfirmDialog(null, "Tambah Transaksi?", "Message", JOptionPane.YES_NO_OPTION);
                if (ok == 0) {
                    tabmode = (DefaultTableModel) tabel_bahanMasuk.getModel();
                    Vector z = new Vector();
                    z.add(txt_idSuppliyer.getText());
                    z.add(txt_nomerPO.getText());
                    z.add(txt_kodeBahan.getText());
                    z.add(txt_namaBahan.getText());
                    z.add(txt_qty.getText());

                    tabmode.addRow(z);

                    int d = 0;
                    for (int e = 0; e < tabmode.getRowCount(); e++) {
                        d += Integer.parseInt(tabmode.getValueAt(e, 4).toString());
                    }
                    labelTotal.setText("" + d);
                    kosong();
                    aktif();
                }
            }
        }
    }//GEN-LAST:event_btn_tambahActionPerformed

    private void tabel_bahanMasukMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabel_bahanMasukMouseClicked
        // TODO add your handling code here:
        int selectedRow = tabel_bahanMasuk.getSelectedRow();
        DefaultTableModel model = (DefaultTableModel) tabel_bahanMasuk.getModel();
        txt_idSuppliyer.setText(model.getValueAt(selectedRow, 0).toString());
        txt_nomerPO.setText(model.getValueAt(selectedRow, 1).toString());
        txt_kodeBahan.setText(model.getValueAt(selectedRow, 2).toString());
        txt_namaBahan.setText(model.getValueAt(selectedRow, 3).toString());
        txt_qty.setText(model.getValueAt(selectedRow, 4).toString());
        btn_simpan.setEnabled(true);
        btn_tambah.setEnabled(false);
        btn_hapus.setEnabled(true);
        btn_ubah.setEnabled(true);
        txt_qty.requestFocus();
    }//GEN-LAST:event_tabel_bahanMasukMouseClicked

    private void btn_ubahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ubahActionPerformed
        // TODO add your handling code here:
        id_suppliyer = txt_idSuppliyer.getText();
        nomer_po = txt_nomerPO.getText().trim();
        kode = txt_kodeBahan.getText().trim();
        nama = txt_namaBahan.getText().trim();
        qty = txt_qty.getText().trim();
        int kapasitas = Integer.parseInt(qty);
        int total = sisa - kapasitas;

        if (id_suppliyer.isEmpty()) {
            JOptionPane.showMessageDialog(null, "ID Suppliyer tidak boleh kosong!");
            btn_cariSuppliyer.requestFocus();
        } else if (nomer_po.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nomer PO tidak boleh kosong!");
            txt_nomerPO.requestFocus();
        } else if (kode.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Kode tidak boleh kosong!");
            txt_kodeBahan.requestFocus();
        } else if (nama.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Nama tidak boleh kosong!");
        } else if (qty.isEmpty()) {
            JOptionPane.showMessageDialog(null, "Qty tidak boleh kosong!");
            txt_qty.requestFocus();
        } else {
            if (total < 0) {
                JOptionPane.showMessageDialog(null, "Qty melebihi sisa kapasitas loket!");
            } else {
                int ok = JOptionPane.showConfirmDialog(null, "Ubah Transaksi?", "Message", JOptionPane.YES_NO_OPTION);
                if (ok == 0) {
                    int i = tabel_bahanMasuk.getSelectedRow();
                    DefaultTableModel model = (DefaultTableModel) tabel_bahanMasuk.getModel();
                    if (i >= 0) {
                        model.setValueAt(txt_idSuppliyer.getText(), i, 0);
                        model.setValueAt(txt_nomerPO.getText(), i, 1);
                        model.setValueAt(txt_kodeBahan.getText(), i, 2);
                        model.setValueAt(txt_namaBahan.getText(), i, 3);
                        model.setValueAt(txt_qty.getText(), i, 4);
                        JOptionPane.showMessageDialog(null, "Data Berhasil Diubah!");
                        int d = 0;
                        for (int e = 0; e < tabmode.getRowCount(); e++) {
                            d += Integer.parseInt(tabmode.getValueAt(e, 4).toString());
                        }
                        labelTotal.setText("" + d);
                        kosong();
                        aktif();
                    } else {
                        JOptionPane.showMessageDialog(null, "Error");
                    }
                }
            }

        }
    }//GEN-LAST:event_btn_ubahActionPerformed

    private void btn_hapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_hapusActionPerformed
        // TODO add your handling code here:
        int ok = JOptionPane.showConfirmDialog(null, "Hapus Transaksi?", "Message", 2, JOptionPane.YES_NO_OPTION);
        if (ok == 0) {
            int index = tabel_bahanMasuk.getSelectedRow();
            DefaultTableModel tb = (DefaultTableModel) tabel_bahanMasuk.getModel();
            tb.removeRow(index);
            int d = 0;
            for (int e = 0; e < tabmode.getRowCount(); e++) {
                d += Integer.parseInt(tabmode.getValueAt(e, 4).toString());
            }
            labelTotal.setText("" + d);
            kosong();
            aktif();
        }
    }//GEN-LAST:event_btn_hapusActionPerformed

    private void btn_simpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_simpanActionPerformed
        // TODO add your handling code here:
        int baris = tabel_bahanMasuk.getRowCount();
        if (baris == 0) {
            JOptionPane.showMessageDialog(null, "Table Masih Kosong!");
        } else {
            int ok = JOptionPane.showConfirmDialog(null, "Simpan Transaksi?", "Message", 2, JOptionPane.YES_NO_OPTION);
            if (ok == 0) {
                try {
                    int i = 0;
                    PreparedStatement ps;
                    while (i < baris) {

                        String query = "INSERT INTO `transaksi_bahan_m`(`id_transaksi`, `supliyer_id`, `no_po`, `kode_bahan`, `nama_bahan`, `qty`, `user_id`) VALUES ("
                                + "'" + txt_idTransaksi.getText() + "',"
                                + "'" + tabel_bahanMasuk.getValueAt(i, 0) + "',"
                                + "'" + tabel_bahanMasuk.getValueAt(i, 1) + "',"
                                + "'" + tabel_bahanMasuk.getValueAt(i, 2) + "',"
                                + "'" + tabel_bahanMasuk.getValueAt(i, 3) + "',"
                                + "'" + tabel_bahanMasuk.getValueAt(i, 4) + "',"
                                + "'" + id_user + "')";
                        ps = MyConnection.getConnection().prepareStatement(query);
                        ps.executeUpdate();
                        i++;
                    }
                    JOptionPane.showMessageDialog(null, "Data Berhasil Disimpan!");
                    DefaultTableModel tb = (DefaultTableModel) tabel_bahanMasuk.getModel();
                    tb.setRowCount(0);
                    labelTotal.setText("0");
                    printData();
                    txt_idSuppliyer.setText("");
                    kosong();
                    aktif();
                    autonumber();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Gagal! Error : " + e);
                }
            }
        }
    }//GEN-LAST:event_btn_simpanActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cariKode;
    private javax.swing.JButton btn_cariSuppliyer;
    private javax.swing.JButton btn_hapus;
    private javax.swing.JButton btn_simpan;
    private javax.swing.JButton btn_tambah;
    private javax.swing.JButton btn_ubah;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel labelTotal;
    private javax.swing.JTable tabel_bahanMasuk;
    private javax.swing.JTextField txt_idSuppliyer;
    private javax.swing.JTextField txt_idTransaksi;
    private javax.swing.JTextField txt_kodeBahan;
    private javax.swing.JTextField txt_namaBahan;
    private javax.swing.JTextField txt_nomerPO;
    private javax.swing.JTextField txt_qty;
    // End of variables declaration//GEN-END:variables
}
