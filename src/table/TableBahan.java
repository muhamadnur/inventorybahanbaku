/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package table;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import koneksi.MyConnection;
import panel.transaksi.PanelBahanKeluar;
import panel.transaksi.PanelBahanMasuk;
import panel.transaksi.PanelProdukMasuk;

/**
 *
 * @author Admin
 */
public class TableBahan extends javax.swing.JFrame {

    DefaultTableCellRenderer renderer;
    private DefaultTableModel tabmode;
    PreparedStatement ps;
    ResultSet rs;
    public PanelBahanMasuk bahanMasuk = null;
    public PanelBahanKeluar bahanKeluar = null;

    /**
     * Creates new form TableStaff
     */
    public TableBahan() {
        initComponents();
        kosong();
        this.setLocationRelativeTo(null);
        datatable();
    }

    private void kosong() {
        txt_cari.setText("");
    }

    private void datatable() {
        String[] Header = {"No.", "Kode Bahan", "Nama Bahan", "Jenis Bahan", "Nomer Loket", "Nama Loket", "Kapasitas", "Bahan Masuk", "Bahan Keluar","Sisa Bahan", "Sisa Kapasitas"};
        tabmode = new DefaultTableModel(Header, 0);
        renderer = (DefaultTableCellRenderer) table_bahan.getTableHeader().getDefaultRenderer();
        renderer.setHorizontalAlignment(JLabel.LEFT);
        String cariitem = txt_cari.getText();
        try {
            String query = "SELECT\n"
                    + "     bahan.`kode` AS bahan_kode,\n"
                    + "     bahan.`nama` AS bahan_nama,\n"
                    + "     bahan.`jenis` AS bahan_jenis,\n"
                    + "     bahan.`loket_id` AS bahan_loket_id,\n"
                    + "     loket.`id` AS loket_id,\n"
                    + "     loket.`nomor` AS loket_nomor,\n"
                    + "     loket.`nama` AS loket_nama,\n"
                    + "     loket.`lokasi` AS loket_lokasi,\n"
                    + "     loket.`kapasitas` AS loket_kapasitas,\n"
                    + "     transaksi_bahan_m.`id` AS transaksi_bahan_m_id,\n"
                    + "     transaksi_bahan_m.`id_transaksi` AS transaksi_bahan_m_id_transaksi,\n"
                    + "     transaksi_bahan_m.`kode_bahan` AS transaksi_bahan_m_kode_bahan,\n"
                    + "     transaksi_bahan_m.`qty` AS transaksi_bahan_m_qty,\n"
                    + "     transaksi_bahan_k.`id` AS transaksi_bahan_k_id,\n"
                    + "     transaksi_bahan_k.`id_transaksi` AS transaksi_bahan_k_id_transaksi,\n"
                    + "     transaksi_bahan_k.`kode_bahan` AS transaksi_bahan_k_kode_bahan,\n"
                    + "     transaksi_bahan_k.`qty` AS transaksi_bahan_k_qty,\n"
                    + "     sum(transaksi_bahan_k.`qty`),\n"
                    + "     sum(transaksi_bahan_m.`qty`)\n"
                    + "FROM\n"
                    + "     `bahan` bahan INNER JOIN `loket` loket ON bahan.`loket_id` = loket.`id`\n"
                    + "     LEFT JOIN `transaksi_bahan_k` transaksi_bahan_k ON bahan.`kode` = transaksi_bahan_k.`kode_bahan`\n"
                    + "     LEFT JOIN `transaksi_bahan_m` transaksi_bahan_m ON bahan.`kode` = transaksi_bahan_m.`kode_bahan`\n"
                    + "WHERE bahan.`nama` like '%" + cariitem + "%'"
                    + "GROUP BY bahan.`kode`";

            ps = MyConnection.getConnection().prepareStatement(query);
            rs = ps.executeQuery(query);
            int no = 1;
            while (rs.next()) {
                Object[] obj = new Object[11];
                obj[0] = Integer.toString(no);
                obj[1] = rs.getString("bahan_kode");
                obj[2] = rs.getString("bahan_nama");
                obj[3] = rs.getString("bahan_jenis");
                obj[4] = rs.getString("loket_nomor");
                obj[5] = rs.getString("loket_nama");
                obj[6] = rs.getString("loket_kapasitas");

                int kapasitas = rs.getInt("loket_kapasitas");
                int bahan_m = rs.getInt("transaksi_bahan_m_qty");
                int bahan_k = rs.getInt("transaksi_bahan_k_qty");
                int sisa = (kapasitas - bahan_m) + bahan_k;
                int sisaBahan = bahan_m - bahan_k;
                obj[7] = bahan_m;
                obj[8] = bahan_k;
                obj[9] = sisaBahan;
                obj[10] = sisa;
                tabmode.addRow(obj);

                no++;
            }
            table_bahan.setModel(tabmode);
            table_bahan.getColumnModel().getColumn(0).setPreferredWidth(30);
            table_bahan.getColumnModel().getColumn(1).setPreferredWidth(100);
            table_bahan.getColumnModel().getColumn(2).setPreferredWidth(150);
            table_bahan.getColumnModel().getColumn(3).setPreferredWidth(120);
            table_bahan.getColumnModel().getColumn(4).setPreferredWidth(100);
            table_bahan.getColumnModel().getColumn(5).setPreferredWidth(120);
            table_bahan.getColumnModel().getColumn(6).setPreferredWidth(100);
            table_bahan.getColumnModel().getColumn(7).setPreferredWidth(100);
            table_bahan.getColumnModel().getColumn(8).setPreferredWidth(100);
            table_bahan.getColumnModel().getColumn(9).setPreferredWidth(100);

            table_bahan.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Failed to call data" + e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        table_bahan = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txt_cari = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabelTitle = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        table_bahan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        table_bahan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                table_bahanMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(table_bahan);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Tabel Bahan");

        txt_cari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_cariKeyReleased(evt);
            }
        });

        jButton1.setText("Cari");
        jButton1.setMaximumSize(new java.awt.Dimension(51, 20));
        jButton1.setMinimumSize(new java.awt.Dimension(51, 20));
        jButton1.setPreferredSize(new java.awt.Dimension(51, 20));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabelTitle.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabelTitle.setText("Data Bahan");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1128, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(10, 10, 10))
            .addComponent(jSeparator1)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jLabelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_cari, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_cariKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_cariKeyReleased
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_txt_cariKeyReleased

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        datatable();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void table_bahanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_table_bahanMouseClicked
        // TODO add your handling code here:
        String title = jLabelTitle.getText();
        int tabelData = table_bahan.getSelectedRow();
        if (title.equals("Data Bahan Masuk")) {
            bahanMasuk.produkID = table_bahan.getValueAt(tabelData, 1).toString();
            bahanMasuk.namaProduk = table_bahan.getValueAt(tabelData, 2).toString();
            bahanMasuk.sisaLoket = Integer.parseInt(table_bahan.getValueAt(tabelData, 10).toString());
            bahanMasuk.itemTerpilihBahan();
            this.dispose();
        }
        if (title.equals("Data Bahan Keluar")) {
            bahanKeluar.produkID = table_bahan.getValueAt(tabelData, 1).toString();
            bahanKeluar.namaProduk = table_bahan.getValueAt(tabelData, 2).toString();
            bahanKeluar.produkMasuk = Integer.parseInt(table_bahan.getValueAt(tabelData, 9).toString());
            bahanKeluar.itemTerpilihBahan();
            this.dispose();
        }

    }//GEN-LAST:event_table_bahanMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TableBahan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TableBahan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TableBahan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TableBahan.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TableBahan().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabelTitle;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable table_bahan;
    private javax.swing.JTextField txt_cari;
    // End of variables declaration//GEN-END:variables
}
