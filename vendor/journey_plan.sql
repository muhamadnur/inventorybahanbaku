-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Agu 2020 pada 11.11
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahan`
--

CREATE TABLE `bahan` (
  `kode` varchar(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `jenis` varchar(128) NOT NULL,
  `loket_id` varchar(11) NOT NULL,
  `user_id` varchar(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `bahan`
--

INSERT INTO `bahan` (`kode`, `nama`, `jenis`, `loket_id`, `user_id`, `created_at`) VALUES
('IDB00000001', 'Testing', 'Item 4', 'IDL00000003', 'ID00000001', '2020-08-10 18:38:15'),
('IDB00000002', 'dagdfde', 'Item 1', 'IDL00000002', 'ID00000001', '2020-08-10 18:33:35'),
('IDB00000003', 'Coba', 'Item 2', 'IDL00000003', 'ID00000001', '2020-08-10 18:36:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kemasan`
--

CREATE TABLE `kemasan` (
  `id` varchar(11) NOT NULL,
  `loket_id` varchar(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `jenis` varchar(128) NOT NULL,
  `user_id` varchar(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kemasan`
--

INSERT INTO `kemasan` (`id`, `loket_id`, `nama`, `jenis`, `user_id`, `created_at`) VALUES
('IDK00000001', 'IDL00000002', 'Testing', 'L-Men', 'ID00000001', '2020-08-10 17:51:52'),
('IDK00000002', 'IDL00000001', 'Makanan Ringan', 'Tropical Slim', 'ID00000001', '2020-08-10 18:12:02'),
('IDK00000003', 'IDL00000003', 'Plastik', 'Nutrisari', 'ID00000001', '2020-08-10 18:10:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `loket`
--

CREATE TABLE `loket` (
  `id` varchar(11) NOT NULL,
  `nomor` varchar(20) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `lokasi` text NOT NULL,
  `kapasitas` int(11) NOT NULL,
  `user_id` varchar(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `loket`
--

INSERT INTO `loket` (`id`, `nomor`, `nama`, `lokasi`, `kapasitas`, `user_id`, `created_at`) VALUES
('IDL00000001', '2131', 'Loket A', 'Plant A', 200, 'ID00000001', '2020-08-09 21:19:31'),
('IDL00000002', '123', 'Loket B', 'Plant B', 300, 'ID00000001', '2020-08-10 22:11:25'),
('IDL00000003', '2345', 'Loket C', 'Plant A', 500, 'ID00000001', '2020-08-10 06:12:56');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_id`
--

CREATE TABLE `role_id` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `role_id`
--

INSERT INTO `role_id` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Staff Gudang'),
(3, 'Staff Produksi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suppliyer`
--

CREATE TABLE `suppliyer` (
  `id` varchar(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `user_id` varchar(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `suppliyer`
--

INSERT INTO `suppliyer` (`id`, `name`, `phone`, `alamat`, `user_id`, `created_at`) VALUES
('IDS00000001', 'PT. Sinar Mulia', '0823423948', 'Jl. Panglima Polim XII', 'ID00000001', '2020-07-10 20:15:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_bahan_k`
--

CREATE TABLE `transaksi_bahan_k` (
  `id` int(11) NOT NULL,
  `id_transaksi` varchar(11) NOT NULL,
  `staff_id` varchar(11) NOT NULL,
  `kode_bahan` varchar(11) NOT NULL,
  `nama_bahan` varchar(128) NOT NULL,
  `qty` int(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `user_id` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi_bahan_k`
--

INSERT INTO `transaksi_bahan_k` (`id`, `id_transaksi`, `staff_id`, `kode_bahan`, `nama_bahan`, `qty`, `created_date`, `user_id`) VALUES
(1, 'IBK00000001', 'ID00000001', 'IDB00000003', 'Coba', 250, '2020-08-10 20:32:49', 'ID00000001'),
(3, 'IBK00000003', 'ID00000001', 'IDB00000001', 'Testing', 50, '2020-08-11 21:21:52', 'ID00000001'),
(4, 'IBK00000003', 'ID00000001', 'IDB00000001', 'Testing', 100, '2020-08-11 21:21:52', 'ID00000001');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_bahan_m`
--

CREATE TABLE `transaksi_bahan_m` (
  `id` int(11) NOT NULL,
  `id_transaksi` varchar(11) NOT NULL,
  `supliyer_id` varchar(11) NOT NULL,
  `no_po` varchar(50) NOT NULL,
  `kode_bahan` varchar(11) NOT NULL,
  `nama_bahan` varchar(128) NOT NULL,
  `qty` int(11) NOT NULL,
  `user_id` varchar(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi_bahan_m`
--

INSERT INTO `transaksi_bahan_m` (`id`, `id_transaksi`, `supliyer_id`, `no_po`, `kode_bahan`, `nama_bahan`, `qty`, `user_id`, `created_date`) VALUES
(1, 'IBM00000001', 'IDS00000001', '342893423809', 'IDB00000002', 'dagdfde', 200, 'ID00000001', '2020-08-10 18:15:45'),
(2, 'IBM00000002', 'IDS00000001', '09328423', 'IDB00000003', 'Coba', 250, 'ID00000001', '2020-08-10 19:47:50'),
(3, 'IBM00000003', 'IDS00000001', '23932', 'IDB00000002', 'dagdfde', 100, 'ID00000001', '2020-08-11 20:09:30'),
(4, 'IBM00000004', 'IDS00000001', '322323', 'IDB00000001', 'Testing', 300, 'null', '2020-08-11 21:19:52'),
(5, 'IBM00000005', 'IDS00000001', '43523423', 'IDB00000003', 'Coba', 500, 'null', '2020-08-11 21:26:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_produk_k`
--

CREATE TABLE `transaksi_produk_k` (
  `id` int(11) NOT NULL,
  `id_transaksi` varchar(11) NOT NULL,
  `produk_id` varchar(11) NOT NULL,
  `nama_produk` varchar(128) NOT NULL,
  `qty` int(11) NOT NULL,
  `user_id` varchar(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi_produk_k`
--

INSERT INTO `transaksi_produk_k` (`id`, `id_transaksi`, `produk_id`, `nama_produk`, `qty`, `user_id`, `created_date`) VALUES
(1, 'IPM00000001', 'IDK00000001', 'asdfadsf', 10, 'ID00000001', '2020-08-09 21:51:20'),
(2, 'IPK00000002', 'IDK00000002', 'asdfadsf', 27, 'ID00000001', '2020-08-10 17:38:18'),
(3, 'IPK00000003', 'IDK00000001', 'Testing', 23, 'ID00000001', '2020-08-10 21:18:49');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_produk_m`
--

CREATE TABLE `transaksi_produk_m` (
  `id` int(11) NOT NULL,
  `id_transaksi` varchar(11) NOT NULL,
  `staff_id` varchar(11) NOT NULL,
  `produk_id` varchar(11) NOT NULL,
  `nama_produk` varchar(128) NOT NULL,
  `qty` int(11) NOT NULL,
  `user_id` varchar(11) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi_produk_m`
--

INSERT INTO `transaksi_produk_m` (`id`, `id_transaksi`, `staff_id`, `produk_id`, `nama_produk`, `qty`, `user_id`, `created_date`) VALUES
(1, 'IPM00000001', 'ID00000001', 'IDK00000001', 'Testing', 23, 'ID00000001', '2020-08-09 18:57:12'),
(2, 'IPM00000001', 'ID00000001', 'IDK00000002', 'asdfadsf', 50, 'ID00000001', '2020-08-09 18:57:12'),
(3, 'IPM00000002', 'ID00000001', 'IDK00000001', 'Testing', 20, 'ID00000001', '2020-08-09 21:51:20'),
(4, 'IPM00000003', 'ID00000001', 'IDK00000001', 'Testing', 77, 'ID00000001', '2020-08-10 21:06:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` varchar(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `is_active` int(1) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `name`, `username`, `password`, `created_at`, `is_active`, `role_id`) VALUES
('ID00000001', 'Administrator', 'admin', 'admin', '2020-08-11 08:31:33', 1, 3);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `bahan`
--
ALTER TABLE `bahan`
  ADD PRIMARY KEY (`kode`);

--
-- Indeks untuk tabel `kemasan`
--
ALTER TABLE `kemasan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `loket`
--
ALTER TABLE `loket`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_id`
--
ALTER TABLE `role_id`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `suppliyer`
--
ALTER TABLE `suppliyer`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi_bahan_k`
--
ALTER TABLE `transaksi_bahan_k`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi_bahan_m`
--
ALTER TABLE `transaksi_bahan_m`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi_produk_k`
--
ALTER TABLE `transaksi_produk_k`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transaksi_produk_m`
--
ALTER TABLE `transaksi_produk_m`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `role_id`
--
ALTER TABLE `role_id`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `transaksi_bahan_k`
--
ALTER TABLE `transaksi_bahan_k`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `transaksi_bahan_m`
--
ALTER TABLE `transaksi_bahan_m`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `transaksi_produk_k`
--
ALTER TABLE `transaksi_produk_k`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `transaksi_produk_m`
--
ALTER TABLE `transaksi_produk_m`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
